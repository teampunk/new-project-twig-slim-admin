<?php 
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1); 
require 'vendor/autoload.php'; 
Twig_Autoloader::register();  
$app = new \Slim\Slim();  
$loader = new Twig_Loader_Filesystem('templates');  
$twig = new Twig_Environment($loader, array(  /*'cache' => 'cache',*/ ));  
//BASE URL
session_start();

define('BASE_URL', 'http://localhost:8080/new-project-twig-slim-admin/');  //Sobreescribir por la ruta de su proyecto.
$data=array(
	'BASE_URL' => constant('BASE_URL'),
);

$app->get('/', function() use ($twig,$data) { 
	$data["my_title"] = "home";
    echo $twig->render('home.html',$data);  
});

$app->get('/about', function() use ($twig,$data){  
    $data["my_title"] = "about";
    echo $twig->render('home.html',$data);  
});

require_once("admin/admin.php");

$app->run();  
?>