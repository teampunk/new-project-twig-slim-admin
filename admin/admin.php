<?php 
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
$data=array(
	'BASE_URL' => constant('BASE_URL'),
	'ADMIN_BASE_URL' => constant('BASE_URL')."admin/"
);
$loaderadmin = new Twig_Loader_Filesystem('admin/templates');  
$twigadmin = new Twig_Environment($loaderadmin, array(  /*'cache' => 'cache',*/ ));  

$app->get('/admin/', function() use ($twigadmin,$app,$data){  
    if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"])){
		$app->response->redirect($app->urlFor('login'), 303);
	}
	else{
		$data['session']= $_SESSION["user_session"];
		echo $twigadmin->render('home.html',$data);
	}    
})->name('admin');


$app->map('/admin/login/', function () use ($twigadmin,$app,$data) {
    $message=0;
    if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"])){
		if($app->request()->isPost()) {
	    	include 'clases/user.php';
	    	$message = User::Login($_POST["username"], $_POST["password"]);
	    	if ($message == "success") {
	    		$app->response->redirect($app->urlFor('admin'), 303);
	    	}
	    	else{
	    		$data['error'] = $message;
	    	}	
    	}
		echo $twigadmin->render('login.html',$data);
	}
	else{
		$app->response->redirect($app->urlFor('admin'), 303);
	}	
})->via('GET', 'POST')->name('login');


$app->get('/admin/logout/', function() use ($twigadmin,$app,$data){  
    include 'clases/user.php';
	User::Logout();
	$app->response->redirect($app->urlFor('login'), 303);
})->name('logout');


$app->map('/admin/register/', function() use ($twigadmin,$app,$data){  
	if (!isset($_SESSION["user_session"]) || empty($_SESSION["user_session"]) || !$_SESSION["user_session"]["is_admin"]){
    	$app->response->redirect($app->urlFor('login'), 303);
	}
	else{
	  	if($app->request()->isPost()) {
			include 'clases/user.php';
	    	$error = User::Register($_POST["usuario"], $_POST["password"], $_POST["password1"]);
	    	$data['error']=$error;
		}
		$data['session']= $_SESSION["user_session"];
		echo $twigadmin->render('register.html',$data);
  	}
})->via('GET', 'POST')->name('register');
 ?>